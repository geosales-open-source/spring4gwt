[![](https://jitpack.io/v/com.gitlab.geosales-open-source/spring4gwt.svg)](https://jitpack.io/#com.gitlab.geosales-open-source/spring4gwt)

# Spring4gwt

Spring4gwt is an open-source project that provides transparent 
integration between Spring and GWT projects on both the client 
and server. It is licensed under the Apache License Version 2.0
(see included license.txt).

This project is a fork from Heptet Group's Spring4GWT. Check
their original repository: https://github.com/heptetgroup/spring4gwt

The folder contains a Maven Reactor. To build spring4gwt dependency,
run:

```bash
./mvnw -pl :spring4gwt package
```

To enable a peek into the samples, activate the `examples` profile.
To compile the spring4gwt and the samples, just run the following:

```bash
./mvnw -P examples compile
```